const fs = require(`fs-extra`);
const imagemin = require(`imagemin`);
const imageminMozjpeg = require(`imagemin-mozjpeg`);
const sharp = require(`sharp`);
const klaw = require('klaw-sync');
const path = require('path');

// Try to enable the use of SIMD instructions. Seems to provide a smallish
// speedup on resizing heavy loads (~10%). Sharp disables this feature by
// default as there's been problems with segfaulting in the past but we'll be
// adventurous and see what happens with it on.
sharp.simd(true);

function processFile(file, outputPath) {
    const pipeline = sharp(file);
    pipeline.toBuffer().then(sharpBuffer =>
        imagemin
            .buffer(sharpBuffer, { plugins: [imageminMozjpeg({ quality: 100, progressive: true })] })
            .then(imageminBuffer => fs.writeFile(outputPath, imageminBuffer))
    );
}

function onlyPng(item) {
    const ext = path.extname(item.path);
    const isFile = item.stats.isFile();
    return isFile && ext === '.png';
}

const pngs = [];
// for (const d of ['static', 'content', 'src']) {
//     pngs.push(...klaw(d, { filter: onlyPng, traverseAll: true }).map(i => i.path));
// }

pngs.push(...klaw(path.resolve('../professional-website/content/blog/building-paddock-tech-part-2/images/'), { filter: onlyPng, traverseAll: true }).map(i => i.path));

for (const png of pngs) {
    const basename = path.basename(png);
    processFile(png, png.replace(basename, `${basename.split('.')[0]}.jpeg`));
}

// const onlyJpeg = through2.obj(function(item, enc, next) {
//     const ext = path.extname(item.path);
//     const isFile = item.stats.isFile();
//     if (isFile && ext === '.png') this.push(item);
//     next();
// });

// function convertPngToJpeg(file) {
//     const basename = path.basename(file);
//     processFile(file, file.replace(basename, `${basename.split('.')[0]}.jpeg`));
// }

// const staticImages = [];
// const contentImages = [];
// const srcImages = [];

// Promise.all([
//     klaw(path.resolve('./static'))
//         .pipe(onlyJpeg)
//         .on('data', item => staticImages.push(item.path))
//         .on('end', () => staticImages.map(convertPngToJpeg)),

//     klaw(path.resolve('./content'))
//         .pipe(onlyJpeg)
//         .on('data', item => contentImages.push(item.path))
//         .on('end', () => contentImages.map(convertPngToJpeg)),

//     klaw(path.resolve('./src'))
//         .pipe(onlyJpeg)
//         .on('data', item => srcImages.push(item.path))
//         .on('end', () => srcImages.map(convertPngToJpeg))
// ]);


// klaw(path.resolve('./src'))
//     .pipe(onlyJpeg)
//     .on('data', item => items.push(item.path))
//     .on('end', () => console.dir(items)); // => total of all pngs (bytes)

// console.log(items);

// klaw(path.resolve('./content'))
//     .pipe(onlyJpeg)
//     .on('data', item => items.push(item.path))
//     .on('end', () => console.dir(items)); // => total of all pngs (bytes)
