---
title: Paddock Tech Progress — April 2019
spoiler: Graphs, graphs everywhere...
description: How far we've come in April 2019 and what is coming in May.
shareMessage: Lots of progress was made on Paddock Tech's GraphQL tech in 2019.
publishDate: 2019-04-01
modifiedDate: 2019-04-01
author: johnrichter
hero: ./images/hero.jpg
hashtags:
  - graphql
  - entrepreneurship
  - motorsport
tags:
  - Paddock Tech
  - Engineering
---

## Accomplishments

The app now has an internal page routing system so that navigating around the app behaves just like a regular website or mobile app. When you click on links, avatars, logos, and buttons they go to the page you expect 🎉

![Navigating around the app](./images/Mobile-Navigation.gif)
![Navigating around the app](./images/Mobile-Navigation2.gif)

## Technical

I spent quite a bit of time learning and familiarizing myself with [GraphQL](https://graphql.org/). I will be using it to fetch the types of data that we'll need across the app and backend services.

### A brief overview of what GraphQL is

I've been interested in this technology ever since I started working with the Facebook GraphAPI at my last job. The concept is rather simple &mdash; a single source of truth for all of your data across a company, its products, and its customers. This is beneficial for a few reasons.

* It unifies business language across teams, developers, and leadership.
* It improves data security as all authentication and authorization logic happens in one place and is consistent.
* Developers across an organization only need to learn one API for everything.
* If the API is public, it forces [Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food).
* Versioning of a GraphQL API is not necessary, unlike a [REST API](https://en.wikipedia.org/wiki/Representational_state_transfer).

For example, given the following API definition

```graphql
type Track {
    id: ID!
    name: string!
    latitude: number!
    longitude: number!
}

type Query {
    tracks: [Track!]!
}
```

you could query it like this

```graphql
query {
    tracks {
        id
        name
        latitude
        longitude
    }
}
```

and get back

```json
{
    "data": {
        tracks: [{
            id: "track-VIR",
            name: "VIR",
            latitude: 36.560008,
            longitude: -79.2047947
        }, {
            id: "track-WGI",
            name: "WGI",
            latitude: 42.3382811,
            longitude: -76.9285464
        }]
    }
}
```

which you could load into an app or website to display a list of tracks. The output matches the structure of the query making it easy know exactly what shape the data from the server will be in, unlike you'd see in a REST API. On the backend the GraphQL server queries any databases, internal or external services, and any other data store required to fulfill the request.

Hopefully you can see how this type of interface to my data will be extremely helpful, especially for a workforce of one.

### The state of the GraphQL ecosystem

GraphQL was released in 2015 by Facebook as a [general specification](https://graphql.github.io/graphql-spec/) with a reference implementation in Javascript which is really not ideal for hosting a web server for a variety of reasons. Given the GraphQL spec didn't define any standards or tooling for building APIs with it, the ecosystem fragmented. The open source community created a **ton** of libraries and tools for both front and backend applications (e.g. command line interfaces and code generators) to make developing and using a GraphQL API easier and more simple.

The rate of adoption, evolution, and support of the tools by larger commercial entities like AirBnB and GitHub has a large influential effect on the ecosystem as a whole which is positive, but getting started with GraphQL is still hard and documentation is still sparse. Tip: always look for recorded conference talks in addition to blogs and open source tool documentation. You can find a lot of information that way.

### Using GraphQL

I attempted to model the company's domain language (`Venues`, `Tracks`, `Events`, etc) in the GraphQL schema language and while it was easy to do conceptually, it wasn't going to work in practice. I wanted to modularize my code, but the GraphQL spec requires all code in one file. This would require me to write my own scripts to combine everything or learn several open source tools to combine and generate code for me. For an example of why this is impractical see [GitHub's schema](https://developer.github.com/v4/public_schema/) (**25,953 lines long**) and imagine trying to maintain that.

The good news is that there are a few all-in-one solutions which enabled me to modularize my GraphQL schemas and generate the final production version. I settled on [TypeGraphQL](https://typegraphql.ml/) and converted my previous schemas to this library with great success. I'm ready to move onto building a test server (which I can do with TypeGraphQL 🎉) with real data that I can query and iterate on as I'm sure I'll be tweaking the schemas as I go.

## What's the plan for May?

I'm going to be implementing a GraphQL API server that will be the place the [web app](/projects/trackside/) will download data from. I should be able to remove the hard coded data from the app and move it to the server. I don't intend to move the data into a database until the app and server are operating and communicating without issues. This is one of the remaining big hurtles before I can get an alpha ready for testers.
