---
title: Building Paddock Tech — Part 5
spoiler: A mobius strip of infinite decisions
description: I show my hand and dive into the design decisions, languages, libraries, infrastructure, and tools that I've been using to develop the Paddock Tech app.
shareMessage: Full stack transparency. What Paddock Tech chose to build their app and why.
publishDate: 2019-03-22
modifiedDate: 2019-03-22
author: johnrichter
hero: ./images/hero.jpg
hashtags:
  - entrepreneurship
  - motorsports
  - fullstack
tags:
  - Paddock Tech
  - Entrepreneurship
  - Engineering
---

## Design Decisions

### Platform

Today you really only have a few options when you want to create a new app built for consumers. Why? Mobile. It is the way a vast majority of the world communicates and derives value. This means that we need to build "mobile-first" and adapt for larger devices such as tablets, laptops, and desktops, but how do we do that?

First we need to define what the terms `web` and `native` mean. A `native` app is one that is built using the platform-specific programming languages and libraries provided by the manufacturer — e.g. Android → Java, Google libs; iPhone → Swift/Objective-C, Apple libs. A `web` app is typically a website built entirely in Javascript that runs entirely on your device. It typically interacts with a server on the internet to download images and other data just like a `native` application would and it also uses your web browser's advanced features to manage any of its settings or current state. You download the app by just visiting a website and won't know you are running an app instead of viewing HTML.

There are two different approaches I could have taken

1. Create both an Android app and an iOS app and submit them to both the Google Play and Apple App store for review and manage our compliance with their store policies. After that, build a website specifically for non-mobile devices. Being smart about it we could use [React-Native](https://facebook.github.io/react-native/) or [Flutter](https://flutter.dev/) to unify the app into one code base (Javascript or DartLang respectively) and compile the code for each mobile platform, but not web.

2. Build a website which is smart enough to adapt to each device and feels like a native application running on the device. Being smart about it we can leverage techniques which allow the website to work offline and further blur the line between a web and native application.

For the app I'm building, I chose option `#2`. This is important because developing and maintaining a single code base that uses a single language is much easier for one person to manage than the 2-3 code bases and languages of option `#1`. This should allow me to rapidly iterate and make changes that everyone can see on any device at the same time &mdash; no slow or staggered feature releases or platform-specific bug fixes to deal with.

It also allows me to leverage the incredibly diverse and hardened web development and design ecosystem that the "smart solutions" to option `#1` lack. That being said I do have several ideas for companion `native` apps that I hope to be able to build in the future.

I want to mention that I don't believe that web apps are a full replacement for real, `native` applications. Users and customers would have better experiences if they were using a `native` app, but for now, until it is a good strategic decision to create them, I need to stick with the one-size-fits-all solution that the `web` provides.

### Languages

* [Typescript](https://www.typescriptlang.org/) — A superset of Javascript which adds a strong typing system to the language. It is Incredibly powerful and eliminates an enormous amount of potential human errors.
* HTML5 — All of the generated content you see in the app is rendered as HTML5
* CSS3 — All of the colors, arrangements of elements in the app, etc are controlled by CSS.
* [GoLang](https://golang.org/) — A super fast, ultra flexible, compiled language that specializes in web services. All of my backend servers will be built with this language. Built and used by Google for a large amount of their critical systems internally.

### Libraries

* [React](https://reactjs.org/) — An extremely popular UI framework. It is extremely flexible, customizable, and performant. If you've heard of [Angular](https://angularjs.org/) or [Vue](https://vuejs.org/), this is a library in the same category.
* [Material UI](https://material-ui.com) — One of my initial struggles was learning the insanity of building simple, yet complex, user interfaces with CSS. I built tons of functionality and had a decent looking website, but most of it was comprised of hacks due to my inexperience. At some point a month or so ago I found the Material UI React library which enabled me build upon this set of rock solid, mind-blowingly customizable set of UI components. The experience I gained from building the entire UI myself was invaluable, but now I can use that knowledge to alter the Material UI components to my specifications quickly so I can keep development at a faster pace.
* [JSS](https://cssinjs.org/) — This is a library that allows me to dynamically create CSS and apply it to specific elements on the page on the fly. Similar to other libraries like [Styled Components](https://www.styled-components.com/) and [Emotion](https://emotion.sh) which are also CSS in Javascript libraries.
* [Font Awesome 5 Pro](https://fontawesome.com/) — One of the best icon sets you can find. They have an incredible free tier, but their pro tier was super affordable and tripled the amount of icon resources I can use.
* [Jest](https://jestjs.io/) — Powers all of the testing I run on the app while I'm developing and before I release a new version of the app.
* [Luxon](https://moment.github.io/luxon/) — A powerful date and time management and calculation library. It handles timezones and locales really well which makes it a godsend for what I'm trying to do.
* [Mobx](https://mobx.js.org/) — A state-management library that serves as a replacement for React's internal state management. The first several iterations of the app were built solely with React's state management tools, but once complexity reached a certain point I had to switch to something more advanced. Mobx fit the case while being unobtrusive and required much less code to use properly. I think I deleted ~30% of the app's code after I switched to this library. 🎉
* [SVGO](https://github.com/svg/svgo) — An SVG file optimizer. Reduces the size of SVG images by removing any code that is not necessary for use on the web. Tip: SVG files are just fancy XML. I use this to make the track maps as small as possible to so the app is only downloading what it needs to display track maps properly.
* [GraphQL](https://graphql.org/) — As the name says this is a Query Language (QL) rather than a library. I'm intending to use it as the basis for the API server I build that will allow the website to pull data from the PostgreSQL database. I'm undecided on what UI and what server library to use so updates to come in the future.

### Infrastructure

I'm still building the infrastructure needed to host the app so some of these are speculative.

* [GitLab](https://gitlab.com) — This is [GitHub](https://github.com) on steroids. I currently have private repositories on GitLab where all of the code is stored and I do all of my project management and planning.
* [Digital Ocean](https://www.digitalocean.com/), [Google Cloud](https://cloud.google.com/), [Amazon Web Services](https://aws.amazon.com/) — Cloud hosting for any servers, databases, etc. Right now it is looking like Google Cloud or Digital Ocean for a few reasons
    * I have just under $300 of free promotional credit on Google Cloud that I need to use in the next 250 days. I've been hosting [GTM's Slack points app](/projects/gtm-ctrl/) here and it has been quite reliable.
    * Digital Ocean is extremely cheap and doesn't nickel and dime you like Google Cloud and AWS. One flat fee with an extremely generous network bandwidth allocation without fine print overage charges. Plus they just released a new managed Postgres database offering with free backups that will fit my use case perfectly. I use DigitalOcean for all of my [Outline VPN](https://getoutline.org/) usage too with great success.
* [PostgreSQL](https://www.postgresql.org/) — A simple, battle hardened SQL database with built in support for sharding and scaling so I can easily grow when I need to.
* [Sendy](https://sendy.co/) — A self-hosted email marketing / management app. Replaces things like Mailchimp for a fraction of the cost. I want to use it to communicate with users and stakeholders in the future.

### Tools

I use a plethora of additional software that helps me project manage, document, design, and perform QA.

* [Notion](https://notion.so) — A do it all knowledge base and documentation app. The quality and feature set has made it dead simple to create detailed documentation on my company, vision, strategy, app, and so much more. You could implement a CRM within the app if you wanted to. Similar to [AirTable](https://airtable.com/) or [Quip](https://quip.com/).
* [Sketch](https://www.sketchapp.com/) —  A tool for designers to build and mock website designs. It is kind of like an extremely specialized and versatile Photoshop built for UI/UX creation and iteration.
* [Affinity Designer](https://affinity.serif.com/en-us/designer/) — A vector graphic application that competes with Adobe Illustrator. There is an equally amazing iPad app that you can use like a professional Wacom tablet. I use this to create the track map graphics used throughout the website.
* [Yarn](https://yarnpkg.com/en/) — Just a simple package manager for the Javascript ecosystem.
* [Git](https://git-scm.com/) — Source code version tracking.
* [VSCode](https://code.visualstudio.com/) — Arguably the best Integrated Development Environment for software engineering at the moment.
* [Create React App](https://facebook.github.io/create-react-app/) — This is the professional build pipeline made by the same team that built React.
* [Babel](https://babeljs.io/) — A tool that is used by `Create React App` to compile my app for as many web browsers as possible, even ones released several years ago.

That's all for this series. I'm happy to answer any questions or go into more detail any subjects that might be of interest. Just message me on [Twitter](https://twitter.com/jrichte43).

Going forward I plan on sharing my monthly goals, the progress I'm making, roadblocks I've run into, how I'm measuring up against my deadlines, and more. My hope is that it builds your trust in me and the final product, helps shape the app into something actually useful for everyone, and gets people thinking about the new possibilities that technology can enable in our sport.

<p style="text-align: center;">🏎💨🏁</p>