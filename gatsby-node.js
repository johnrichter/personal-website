const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');
const blogUtils = require('./src/lib/blogUtils');
const redirects = require('./src/config/gatsby/redirects');
const routes = require('./src/config/routes');

module.exports = {
    createSchemaCustomization({ actions }) {
        // We auto link authors from blog posts to the authors.yaml structure here.
        actions.createTypes(`
            type MarkdownRemark implements Node { frontmatter: MarkdownRemarkFrontmatter }
            type MarkdownRemarkFrontmatter {
                author: AuthorYaml @link(by: "aid")
            }
            type AuthorYaml implements Node {
                posts: [MarkdownRemark] @link(from: "aid", by: "frontmatter.author.aid")
            }
        `);
    },
    onCreateNode({ node, getNode, actions }) {
        if (node.internal.type === 'MarkdownRemark') {
            const slug = createFilePath({ node, getNode, basePath: 'content', trailingSlash: true });
            actions.createNodeField({ node, name: 'slug', value: slug });
        }
    },
    async createPages({ graphql, actions }) {
        const { createPage, createRedirect } = actions;

        // Global redirects
        for (const redirect of redirects) {
            createRedirect(redirect);
        }

        // Blog posts
        const markdown = graphql(`
            query {
                allMarkdownRemark(
                    sort: { fields: [frontmatter___publishDate], order: ASC },
                    filter: {fields: {slug: {glob: "${routes.BLOG_PATH_PREFIX}*"}}}
                ) {
                    edges {
                        node {
                            fields {
                               slug
                            }
                            frontmatter {
                                title
                            }
                        }
                    }
                }
                tagGroups: allMarkdownRemark {
                    group(field: frontmatter___tags) {
                        fieldValue
                    }
                }
            }
        `).then(result => {
            if (result.errors) {
                throw result.errors;
            }
            // Create post pages
            const posts = result.data.allMarkdownRemark.edges.map(e => e.node);
            for (const [index, post] of posts.entries()) {
                let previousPost = null;
                if (index > 0) {
                    previousPost = posts[index - 1];
                }
                let nextPost = null;
                if (index <= (posts.length - 1)) {
                    nextPost = posts[index + 1];
                }
                createPage({
                    path: post.fields.slug,
                    component: path.resolve('./src/templates/blog/post.tsx'),
                    context: {
                        slug: post.fields.slug,
                        previousPost,
                        nextPost
                    }
                });
            }
            // Create pages for each blog topic/tag
            const tagGroups = result.data.tagGroups.group;
            for (const tag of tagGroups) {
                const slug = blogUtils.tagPath(tag.fieldValue);
                createPage({
                    path: slug,
                    component: path.resolve('./src/templates/blog/tag.tsx'),
                    context: { tag: tag.fieldValue, slug }
                });
            }
        });
        return Promise.all([markdown]);
    }
};
