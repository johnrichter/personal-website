# Notice

Please see [FONTS.md](../../../FONTS.md)

# Retrieve fonts

To download fonts use

https://github.com/neverpanic/google-font-download

and run this script (at least on my machine)

```bash
PATH="/usr/local/opt/gnu-getopt/bin:/usr/local/opt/grep/libexec/gnubin:$PATH" ./google-font-download --url="https://fonts.googleapis.com/css?display=swap&family=Exo:300,300i,400,400i,600,600i,700,700i"
```

Copy the css and files into this folder (`src/assets/fonts`). If they are new fonts, and we need to use them. Add an
`@import url("")` to `./fonts.css`. Add `display: swap` to the font-face definitions. `google-font-download` doesn't do
that for us.

## For more info checkout

https://neverpanic.de/blog/2014/03/19/downloading-google-web-fonts-for-local-hosting/
https://mranftl.com/2014/12/23/self-hosting-google-web-fonts/
