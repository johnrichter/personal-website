export type TBasicVariants = 'email' | 'blogRss';
export type TIdentityVariants = 'keybase';
export type TSocialMediaVariants =
    'linkedin' | 'twitter' | 'facebook' | 'instagram' | 'youtube' | 'spotify' | 'tumblr' | 'fitbit' | 'reddit' |
    'jawbone' | 'runkeeper';
export type TDeveloperVariants = 'github' | 'gitlab';
export type TSocialVariants = TBasicVariants | TIdentityVariants | TSocialMediaVariants | TDeveloperVariants;
export type TExternalSocialVariants = TIdentityVariants | TSocialMediaVariants | TDeveloperVariants;
