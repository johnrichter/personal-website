declare module "*.css" {
    const content: any;
    export default content;
}

declare module "*.eot" {
    const content: any;
    export default content;
}

declare module '*.jpg' {
    const content: any;
    export default content;
}

declare module '*.jpeg' {
    const content: any;
    export default content;
}

declare module '*.png' {
    const content: any;
    export default content;
}

declare module "*.sass" {
    const content: any;
    export default content;
}

declare module "*.scss" {
    const content: any;
    export default content;
}

declare module "*.ttf" {
    const content: any;
    export default content;
}

declare module "*.woff" {
    const content: any;
    export default content;
}

declare module "*.woff2" {
    const content: any;
    export default content;
}

declare module '*.svg' {
    import * as React from 'react';
    // export const ReactComponent: React.FC<React.SVGProps<HTMLOrSVGElement>>;
    export const ReactComponent: React.ComponentType<React.SVGProps<SVGSVGElement>>;
    const loaderPath: string;
    export default loaderPath;
}
