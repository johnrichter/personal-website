import { GenerateId, Jss } from 'jss';
import { StylesProviderProps } from '@material-ui/styles/StylesProvider';

export const JSS_SEED: string;
export const JSS_CLASS_NAME_GENERATOR: GenerateId;
export const JSS_INSTANCE: Jss;
export const STYLES_PROVIDER_PROPS: StylesProviderProps;
export default STYLES_PROVIDER_PROPS;
