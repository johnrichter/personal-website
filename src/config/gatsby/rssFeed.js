
function getBlogFeed({ siteUrl, filePathRegex, blogSlug, ...gatsbyPluginFeedOptions }) {
    return {
        query: `
            {
                allMarkdownRemark(
                    sort: {fields: [frontmatter___publishDate], order: DESC },
                    filter: {fileAbsolutePath: {regex: "${filePathRegex}"}}
                ) {
                    edges {
                        node {
                            fields {
                                slug
                            }
                            frontmatter {
                                title
                                date: publishDate
                                description: spoiler
                                categories: tags
                                author {
                                    name
                                }
                            }
                            html
                        }
                    }
                }
            }
        `,
        serialize({ query: { allMarkdownRemark } }) {
            const blogUrl = `${siteUrl}${blogSlug}`;
            return allMarkdownRemark.edges.map(edge => {
                const url = `${siteUrl}${edge.node.fields.slug}`;
                const footer = `
                    <div style="margin-top=55px; font-style: bold;">
                        (This article was posted to John Richter's blog at <a href="${blogUrl}">${blogUrl}</a>. You can
                        <a href="${url}">read it online by clicking here</a>.)
                    </div>
                `;
                // Hacky workaround for https://github.com/gaearon/overreacted.io/issues/65
                const html = (edge.node.html || ``)
                    .replace(/href="\//g, `href="${siteUrl}/`)
                    .replace(/src="\//g, `src="${siteUrl}/`)
                    .replace(/"\/static\//g, `"${siteUrl}/static/`)
                    .replace(/,\s*\/static\//g, `,${siteUrl}/static/`);

                return {
                    ...edge.node.frontmatter,
                    author: edge.node.frontmatter.author.name,
                    url,
                    guid: url,
                    custom_elements: [
                        {
                            'content:encoded': `
                                <div style="width: 100%; margin: 0 auto; max-width: 42rem; padding: 16px;">
                                    ${html}
                                    ${footer}
                                </div>
                            `,
                        },
                    ],
                };
            });
        },
        ...gatsbyPluginFeedOptions
    };
}

module.exports = { getBlogFeed };
