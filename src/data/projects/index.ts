
export type TProjectWork = {
    slug: string;
    category: string;
    type: string;
    title: string;
    spoiler: string;
    description: string;
    stack: string[];
    links: {
        repository: string; homepage: string;
    };
};
