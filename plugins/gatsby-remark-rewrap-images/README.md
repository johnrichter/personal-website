

# Based on gatsby-remark-unwrap-images and gatsby-remark-images

https://github.com/cedricdelpoux/gatsby-remark-unwrap-images

From the `gatsby-remark-unwrap-images` readme:

## Usecase

`gatsby-remark-rewrap-images` is usefull if your want to style _paragraphs_ and _images_ differently.

### With `gatsby-remark-rewrap-images`

```md
Hello Word!

![](image1.jpg)
![](image2.jpg)
```

The previous markdown will generate the following DOM:

```html
<div>
  <p>Hello Word!</p>
  <img src="image1.jpg" />
  <img src="image2.jpg" />
</div>
```

### Without `gatsby-remark-rewrap-images`

```md
Hello Word!

![](image1.jpg)
![](image2.jpg)
```

The previous markdown will generate the following DOM:

```html
<div>
  <p>Hello Word!</p>
  <p>
    <img src="image1.jpg" />
    <img src="image2.jpg" />
  </p>
</div>
```
