const visit = require('unist-util-visit');
const remove = require('unist-util-remove');
const fs = require('fs');
const path = require('path');
const imageSize = require('probe-image-size');
const _ = require('lodash');

const MDAST_IMAGE_TYPES = ['image', 'imageReference'];
const IMAGES_CONTAINER_CLASS = 'gatsby-remark-rewrap-images-container';
const IMAGE_ITEM_CLASS = 'gatsby-remark-rewrap-images-image-item';
const IMAGE_CONTAINER_CLASS = 'gatsby-remark-rewrap-images-image-container';
const IMAGE_LINK_CLASS = 'gatsby-remark-rewrap-images-image-link';
const IMAGE_BACKGROUND_CLASS = 'gatsby-remark-rewrap-images-image-background';
const IMAGE_CLASS = 'gatsby-remark-rewrap-images-image';

module.exports = ({ markdownAST }, pluginOptions) => {
    // A lot of this code came from gatsby-remark-images, but has been modified to fit my usecase. Meant to keep image
    // loading consistent across my plugin and theirs. Enables me to make images into a grid by targeting the classnames
    visit(markdownAST, 'paragraph', (node, index, parent) => {
        const hasOnlyImagesNodes = node.children.every(child => (
            MDAST_IMAGE_TYPES.includes(child.type) ||
            (child.type === 'text' && child.value === '\n') ||
            // This is kind of hacky, but should work. Just looking for the class name prefix
            (child.type === 'html' && child.value.substr(0, 50).includes('gatsby-resp-image'))
        ));
        if (!hasOnlyImagesNodes) return;

        remove(node, 'text');
        const htmlImageNodes = [];

        for (const imgNode of node.children) {
            if (imgNode.type === 'html' && imgNode.value.substr(0, 50).includes('gatsby-resp-image')) {
                // Relies this being set in gatsby-config.js via `wrapperStyle` callback in gatsby-remark-images options
                const flex = imgNode.value
                    .replace(/\n/g, '')
                    .match(/^.*gatsby-resp-image.*?"\s+style=".*(flex:.*?;).*?"/)[1] || '';
                htmlImageNodes.push(`<div class="${IMAGE_ITEM_CLASS}" style="${flex}">${imgNode.value}</div>`.trim());
                continue;
            };
            // Only handle absolute paths and gifs. Relies on remark-copy-linked-files for abs paths
            if (imgNode.url.startsWith('./') || !imgNode.url.endsWith('.gif')) continue;

            const publicPath = path.resolve(path.join('public', imgNode.url));
            const pathParts = publicPath.split('/');
            const imgFile = pathParts[pathParts.length - 1];
            const imgFileName = imgFile.replace(/\.[^/.]+$/, '');
            const { width, height } = imageSize.sync(Array.from(fs.readFileSync(publicPath)));
            const defaultAlt = imgFileName.replace(/[^A-Z0-9]/gi, ' ');
            const alt = _.escape(imgNode.alt || defaultAlt);
            const title = imgNode.title ? _.escape(imgNode.title) : alt;
            htmlImageNodes.push(`
                <div class="${IMAGE_ITEM_CLASS}" style="flex:${(width / height)} 1 0%;">
                    <span
                        class="${IMAGE_CONTAINER_CLASS}"
                        style="position:relative;display:block;margin-left:auto;margin-right:auto;max-width:${pluginOptions.maxWidth}px;"
                    >
                        <a
                            class="${IMAGE_LINK_CLASS}"
                            href="${imgNode.url}"
                            style="display: block"
                            target="_blank"
                            rel="noopener"
                        >
                            <span
                                class="${IMAGE_BACKGROUND_CLASS}"
                                style="padding-bottom:${(height / width) * 100}%;position:relative;bottom:0;left:0;display:block;"
                            ></span>
                            <img
                                class="${IMAGE_CLASS}"
                                alt="${alt}"
                                title="${title}"
                                src="${imgNode.url}"
                                loading="lazy"
                            />
                        </a>
                    </span>
                </div>
            `.trim());
        }
        const rawHTML = `
            <div class="${IMAGES_CONTAINER_CLASS}" style="display:flex;justify-content:center">
                ${htmlImageNodes.join('')}
            </div>
        `.trim();
        node.type = 'html';
        node.value = rawHTML;
    });


    ''.match();














    // console.log(files);
    // visit(markdownAST, 'paragraph', (node, index, parent) => {
    //     const hasBeenWrapped = !!(node.data && node.data.isWrapper);
    //     const hasOnlyImagesNodes = node.children.every(
    //         child => child.type === 'image' || (child.type === 'text' && child.value === '\n')
    //     );
    //     if (!hasBeenWrapped && hasOnlyImagesNodes) {
    //         remove(node, 'text');
    //         // Create new wrapper node
    //         // const wrapper = {
    //         //     type: 'paragraph',
    //         //     children: [...node.children],
    //         //     data: {
    //         //         isWrapper: true,
    //         //         hProperties: {
    //         //             className: ['gatsby-remark-rewrap-images-wrapper']
    //         //         }
    //         //     }
    //         // };
    //         // console.log('before', parent.children[index]);
    //         // parent.children.splice(index, 1, wrapper);
    //         // console.log('after ', parent.children[index]);
    //         // return index;
    //         parent.children[index].data = {
    //             isWrapper: true,
    //             hProperties: {
    //                 className: [WRAPPER_CLASS]
    //             }
    //         };
    //     }
    //     // return undefined;
    // });
    // visit(markdownAST, ['image', 'imageReference'], (node, index, parent) => {
    //     if (parent.data && parent.data.hProperties && parent.data.hProperties.className.includes(WRAPPER_CLASS)) {
    //         if (!node.url.startsWith('./')) {
    //             const publicPath = path.resolve(path.join('public', node.url));
    //             const dimensions = imageSize.sync(Array.from(fs.readFileSync(publicPath)));
    //             const style = `flex: ${dimensions.width / dimensions.height} 1 0%;`;
    //             const hProps = (node.data || {}).hProperties || {};
    //             hProps.style = hProps.style ? `${hProps.style}${style}` : style;
    //             if (node.data) {
    //                 node.data.hProperties = hProps;
    //             } else {
    //                 node.data = { hProperties: hProps };
    //             }
    //             console.log(node);
    //         }

    //         // console.log('node', node);
    //         // console.log('nodeParent', node.parent);
    //         // console.log('parent', parent);
    //         // since dir will be undefined on non-files
    //         // const nodeParent = parent || node.parent;
    //         // if (nodeParent && getNode(nodeParent).internal.type !== 'File') {
    //         //     return;
    //         // }
    //         // const pNode = getNode(nodeParent);
    //         // console.log('pNode', pNode);
    //         // const imagePath = path.posix.join(getNode(parent).dir, node.url);
    //         // const imageNode = files.filter(
    //         //     file => (file && file.absolutePath) ? file.absolutePath === imagePath : false
    //         // );
    //         // if (imageNode && imageNode.url.startsWith('./') && imageNode.absolutePath) {
    //         //     const dimensions = imageSize.sync(Array.from(fs.readFileSync(imageNode.absolutePath)));
    //         //     console.log('dimensions', dimensions);
    //         // }
    //     }
    // });
    return markdownAST;
};
