# What is this

This plugin modifies all of `gatsby-remark-vscode` `<pre>` blocks by wrapping them in a div just like Gatsby
does for prismjs
