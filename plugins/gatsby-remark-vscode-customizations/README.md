You can use this as a local VSCode extension.

1. Make sure list of themes is up to date in `package.json`
2. Create a folder structure `JohnRichter.gatsby-remark-vscode-customizations/<everything in this plugin>`.
3. Move the folder somewhere useful. E.g. `src/lib/plugins`
4. Configure `gatsby-remark-vscode` with `extensions` and `theme`
    ```js
    options: {
        theme: {
            default: 'Material Theme Palenight',
        },
        extensions: [
            `${__dirname}/plugins/gatsby-remark-vscode-customizations/extensions/kumar-harsh.graphql-for-vscode-1.15.3.vsix`
        ]
    }
    ```

I can't remember how I originally built the themes, but I think I extracted them from the vscode extension repos on
github and removed/altered some fields as I saw fit.

## `gatsby-remark-vscode` Extension Cache

This plugin also serves as the cache for the extensions that `gatsby-remark-vscode` downloads. This will avoid rate
limiting by Microsoft's extension repo. Extensions are stored in `./extensions`
